FROM python:3.6-alpine

RUN adduser -D flitter

WORKDIR /home/flitter

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY app app
COPY migrations migrations
COPY flitter.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP flitter.py

RUN chown -R flitter:flitter ./
USER flitter

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
