# flitter [![CircleCI](https://circleci.com/bb/lcernei/flitter/tree/master.svg?style=svg)](https://circleci.com/bb/lcernei/flitter/tree/master)

This is a twitter-like web app,  developed using Flask.

## Getting Started

First time:  
Clone the repository: `git clone ... `  
Change directory to the project: `cd flitter`  
Create a python environment: `python3 -m venv venv`  
(depending of the OS or instalation `python3` should be substituted with `python`, `py3` or `py`)

Every time:  
Activate the environment:  
* Windows - `venv\Scripts\activate`  
* Other - `source venv/bin/activate`  
* Mihai - `source venv/bin/activate.fish`  

To deactivate it use `deactivate`.

### Prerequisites

Python with version > 3.5 should be installed.  
To install the required libraries use `pip install -r requirements.txt`.  
If you added some new libraries during development, include them in the `requirements.txt` with `pip freeze > requirements.txt`  
Also install Docker to test deployment on docker.

### Usage

To run the project, use `flask run`, then open a web browser with the link [localhost:5000](http://127.0.0.1:5000/).  
Docker:  
`docker build -t flitter:latest .` - to build the image (only the first time)  
`docker images` - to see the previously created images  
`docker run --name flitter -d -p 8000:5000 -v $(pwd)/app.db:/home/flitter/app.db --rm flitter:latest` - to run the container (with db sync)  
`docker ps` - to see the running containers  
`docker stop <CONTAINER ID>` - to stop the container (also removes it)  
